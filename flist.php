<?php

/**
 * Wrapper for Facility Search Results
 */

class FList
{
    
    public $cent_lat, $cent_long, $display_results_total_count_for_page, $map_results, $flist_results, $total_results, $total_page;
    public function __construct($map_q="48186", $page=1, $distance=30, $features=array(), $display_number=20) {
        $testdata = $this->getCoordinatesFromAddress($map_q); 
        $ilat =  $testdata->results[0]->geometry->location->lat;
        $ilng =  $testdata->results[0]->geometry->location->lng;
        $pdo_db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 

        $other = FALSE;


        //print_r($feature_query);


        if (!empty($features) ){
            $sql = "SELECT f.*, c.name, c.logo, c.link_source,
                    3956 * 2 * ASIN(SQRT( POWER(SIN(({$ilat} - latitude) * pi()/180 / 2), 2) + COS({$ilat} * pi()/180) * COS(latitude * pi()/180) *POWER(SIN(({$ilng} - longitude) * pi()/180 / 2), 2) )) as distance 
                    FROM facility f, features fe, facility_feature ff, company c 
                    WHERE f.company_id = c.id AND company_id !=5 AND f.id = ff.facility_id AND fe.id = ff.feature_id AND (ff.feature_id = 0"; 
            foreach ($features as $ind_fea){
                $sql .= " OR ff.feature_id = {$ind_fea}";
            
            }        
            $sql .= ") GROUP BY id HAVING distance <= {$distance} ORDER BY distance";    
        }
        else {
            $sql = "SELECT f.*, c.name, c.logo, c.link_source, 
                    3956 * 2 * ASIN(SQRT( POWER(SIN(({$ilat} - latitude) * pi()/180 / 2), 2) + COS({$ilat} * pi()/180) * COS(latitude * pi()/180) *POWER(SIN(({$ilng} - longitude) * pi()/180 / 2), 2) )) as distance 
                    FROM facility f, company c
                    WHERE f.company_id = c.id AND company_id !=5            
                    HAVING distance <= {$distance} ORDER BY distance"; 
            
        }

        $statement = $pdo_db->prepare($sql);
        $statement->execute();
        $this->flist_results = $statement->fetchAll(PDO::FETCH_ASSOC); 
        print_r($this->flist_results[0]);
        $this->cent_lat = $this->flist_results[0]['latitude'];
        //echo $this->cent_lat;
        $this->cent_long = $this->flist_results[0]['longitude'];
        $center_id   = $this->flist_results[0]['id']; 
        $this->total_results = count($this->flist_results);

        if ($this->total_results <1) {
            if (!empty($features) ){
                $sql = "SELECT f.*, c.name, c.logo, c.link_source, 
                        3956 * 2 * ASIN(SQRT( POWER(SIN(({$ilat} - latitude) * pi()/180 / 2), 2) + COS({$ilat} * pi()/180) * COS(latitude * pi()/180) *POWER(SIN(({$ilng} - longitude) * pi()/180 / 2), 2) )) as distance 
                        FROM facility f, features fe, facility_feature ff, company c 
                        WHERE f.company_id = c.id AND company_id AND company_id =5 AND f.id = ff.facility_id AND fe.id = ff.feature_id AND (ff.feature_id = 0";         
                foreach ($features as $ind_fea){
                    $sql .= " OR ff.feature_id = {$ind_fea}";
                
                } 
                $sql .= ") GROUP BY id HAVING distance <= {$distance} ORDER BY distance";    
            }
            else {
                $sql = "SELECT f.*, c.name, c.logo, c.link_source, 
                        3956 * 2 * ASIN(SQRT( POWER(SIN(({$ilat} - latitude) * pi()/180 / 2), 2) + COS({$ilat} * pi()/180) * COS(latitude * pi()/180) *POWER(SIN(({$ilng} - longitude) * pi()/180 / 2), 2) )) as distance 
                        FROM facility f, company c WHERE f.company_id = c.id AND  company_id =5
                        HAVING distance <= {$distance} ORDER BY distance"; 
                
            }
            
            //echo $sql."<br/>"; 
            
            $statement = $pdo_db->prepare($sql);
            $statement->execute();
            $this->flist_results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $this->cent_lat = $this->flist_results[0]['latitude'];
            $this->cent_long = $this->flist_results[0]['longitude'];
            $center_id   = $this->flist_results[0]['id'];    
            $this->total_results = count($this->flist_results);  
            $other = TRUE;         
        }
        //print_r($results);
        //echo $total_results;
        //print_r(array_slice($results,20,40));
        $total_units = count($this->flist_results);
        if (($total_units/20) == floor(($total_units/20))) {
            $this->total_page = floor($total_units/20);    
        } else {
            $this->total_page = floor($total_units/20) + 1;    
        }

        $offset = ($page - 1) * 20; 
        $offset_limit = $offset+20;
        $results = array_slice($this->flist_results,$offset,$offset_limit);
        $this->display_results_total_count_for_page = count($this->flist_results);
        //$map_sql ="select f.*, c.name, c.logo, c.link_source from facility f, company c where f.company_id != 5 ";
        $map_sql = "SELECT f.*, c.name, c.logo, c.link_source, 
                    3956 * 2 * ASIN(SQRT( POWER(SIN(({$ilat} - latitude) * pi()/180 / 2), 2) + COS({$ilat} * pi()/180) * COS(latitude * pi()/180) *POWER(SIN(({$ilng} - longitude) * pi()/180 / 2), 2) )) as distance 
                    FROM facility f, company c
                    WHERE f.company_id = c.id AND company_id !=5";
        if($other) {
            $statement = $pdo_db->prepare($map_sql);
            $statement->execute();
            $this->map_results = $statement->fetchAll(PDO::FETCH_ASSOC);
        } else {
            if($this->display_results_total_count_for_page>0) {
                $map_sql .= " and f.id not in (0 ";
                foreach ($results as $not_in ) {
                    $map_sql .= ", {$not_in['id']}";            
                }
                $map_sql .= ")";
            }
        }            
        $map_sql .="ORDER BY distance limit 1000"; 
        echo $map_sql;

        $statement = $pdo_db->prepare($map_sql);
        $statement->execute();
        $map_query_results = $statement->fetchAll(PDO::FETCH_ASSOC); 

        $this->map_results = array_merge($this->flist_results, $map_query_results);
        $this->cent_lat = $this->map_results[0]['latitude'];
        $this->cent_long = $this->map_results[0]['longitude'];         
    
    }
    public function getCoordinatesFromAddress( $sQuery, $sCountry = 'usa' )
    {
        $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($sQuery).'&sensor=false&region='.$sCountry.'&language='.$sCountry;
        $sData = file_get_contents($sURL);
        
        return json_decode($sData);
    }

    public function getAddressFromCoordinates( $dLatitude, $dLongitude, $sCountry = 'usa' )
    {
        $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$dLatitude,$dLongitude").'&sensor=false&region='.$sCountry.'&language='.$sCountry;
        $sData = file_get_contents($sURL);
        
        return json_decode($sData);
    }

}




$obj = new FList();
$test_features = array(2,4,7,8);
$obj2 = new FList("66223", 1, 50);
print_r($obj2);

?>