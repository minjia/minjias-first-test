<?php
function getSoapInstance()
{ 
    $soap_singleton = NULL; 
    if ( ! $soap_singleton )
      $soap_singleton = new SoapClient("https://slc.centershift.com/store40/SWS.asmx?WSDL", array('trace' => TRUE));
    return $soap_singleton;
}
function doRequest($name, $params = array())
{
    $params = array('LookupUser_Request' => array(
                      'Username' => 'SS3Golocal',
                      'Password' => 'B38%kqy',
                      'Channel'  => 1),
                      'Request'  => $params);


    try
    {
      $resp = getSoapInstance()->$name($params);
    }
    catch ( Exception $e )
    {
        echo "Error!";
        echo $e -> getMessage ();
        echo 'Last response: '. getSoapInstance()->__getLastResponse();
    }



    $result_name = $name .'Result';
    return $resp->$result_name;
}

function getOrgList()
{
  $res = doRequest('GetOrgList');
  return $res;
}

function getSiteList($org_id = NULL)
{
    if ( ! $org_id )
    {
      $org_list = getOrgList();
      $org_id = $org_list->Details->Organization->OrgID;
    }


    $res = doRequest('GetSiteList', array('OrgID' => $org_id));
    return $res;
}

function getUnitData($id, $unit_id=NULL)
{
    $params = array(
    'SiteID' => $id,
    'Active' => 'Y',
    'Status' => 1 //New by JDB on 7/2/2013
    );
    if ( $unit_id )
    $params['UnitID'] = $unit_id;
    $res = doRequest('GetUnitData', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;
}

function getUnitFeatures($id)
{
    $res = doRequest('GetUnitFeatures', array(
    'SiteID' => $id,
    ));
    return $res;
}

function getSingleUnit($site_id, $unit_id)
{
  $res = doRequest('GetUnitData', array(
    'SiteID' => $site_id,
    'UnitID' => $unit_id,
  ));

  return $res;
}

function getSiteDetails($ids_list=array())
{
    $res = doRequest('GetSiteDetails', array('SiteID' => $ids_list));
    return $res;
}

function getCoordinatesFromAddress( $sQuery, $sCountry = 'usa' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($sQuery).'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}

function getAddressFromCoordinates( $dLatitude, $dLongitude, $sCountry = 'it' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$dLatitude,$dLongitude").'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}


//start the update process
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 

/*******start of facility update for securecare*******/  
$result_display = doRequest('GetSiteList', array('OrgID' => 5041));
//print_r($result_display);
$site_list = $result_display->Details->SOA_GET_SITE_LIST;

$totoal_site_count = 0;
$total_site_change_count = 0;
$no_update = array(1000001109,701958,701074,701073,701503);
foreach ($site_list as $isite) {
    $sql_check = "select * from facility where site_id = {$isite->SITE_ID} and company_id = 1";
    $statement = $db->prepare($sql_check);
    $statement->execute();
    $results_check = $statement->fetchAll(PDO::FETCH_ASSOC); 
    $results_check_count = count($results_check);         
    $site_display_name = addslashes($isite->DISPLAY_NAME);
    $site_city = addslashes($isite->CITY);
    $site_address = addslashes($isite->LINE1);
    if (!in_array($isite->SITE_ID, $no_update )) {        
        if ($results_check_count <1) {
            //echo $sql_check;
            //echo "\n";

            $sql_stmt = "insert into facility (company_id, site_id, display_name, site_location_code, address, city, state, zip, 
            phone, email, property_type, site_status, online_status, website) 
                        values ('1', '{$isite->SITE_ID}', '{$site_display_name}', '{$isite->SITE_NUMBER}', '{$site_address}', '{$site_city}', '{$isite->STATE}', 
                        '{$isite->POSTAL_CODE}', '{$isite->PHONE}', '{$isite->EMAIL_ADDRESS}', '{$isite->PROPERTY_TYPE}', '{$isite->SITE_STAUTS}', '{$isite->ONLINE_STATUS}',
                        'www.securcareselfstorage.com')";
            
            
                   
        } else {
            $sql_stmt = "update facility set display_name = '{$site_display_name}', address = '{$site_address}', city =  '{$site_city}', state = '{$isite->STATE}', 
                        zip = '{$isite->POSTAL_CODE}', phone = '{$isite->PHONE}', email = '{$isite->EMAIL_ADDRESS}', property_type ='{$isite->PROPERTY_TYPE}', 
                        site_status = '{$isite->SITE_STAUTS}', online_status = '{$isite->ONLINE_STATUS}' where company_id = 1 and site_id = {$isite->SITE_ID} ";
            //echo "found, no update\n";
            
        }  
        echo $sql_stmt;
        echo "\n";
        $statement = $db->prepare($sql_stmt);
        $statement->execute();
        $total_site_change_count ++;
          
    }   
    $totoal_site_count ++;
}
echo $totoal_site_count;
echo "\n";
$sql_stmt = "update facility set address = '3400 Longmire Drive', city =  'College Station', state = 'TX', 
            zip = '77845' where company_id = 1 and site_id = 500486; ";
$sql_stmt .= "update facility set address = '3007 Longmire Drive', city =  'College Station', state = 'TX', 
            zip = '77845' where company_id = 1 and site_id = 500487; ";
$sql_stmt .= "update facility set address = '4074 State Hwy 6 South', city =  'College Station', state = 'TX', 
            zip = '77845' where company_id = 1 and site_id = 500488; ";            
$sql_stmt .= "update facility set address = '625 South Graham Road', city =  'College Station', state = 'TX', 
            zip = '77845' where company_id = 1 and site_id = 500489; ";            
$sql_stmt .= "update facility set address = '2306 S College Avenue', city =  'Bryan', state = 'TX', 
            zip = '77801' where company_id = 1 and site_id = 500490; ";            
$sql_stmt .= "update facility set address = '1109 Baker Avenue', city =  'Bryan', state = 'TX', 
            zip = '77803' where company_id = 1 and site_id = 500491; ";            
$sql_stmt .= "update facility set address = '831 North Forest Street', city =  'Amarillo', state = 'TX', 
            zip = '79106' where company_id = 1 and site_id = 500376; ";            
$sql_stmt .= "update facility set address = '2328 A Lakeview', city =  'Amarillo', state = 'TX', 
            zip = '79109' where company_id = 1 and site_id = 500374; ";            
$sql_stmt .= "update facility set address = '1005 West Cotton Street', city =  'Longview', state = 'TX', 
            zip = '75604' where company_id = 1 and site_id = 500494; "; 
$sql_stmt .= "update facility set address = '108 Gilmer Road', city =  'Longview', state = 'TX', 
            zip = '75604' where company_id = 1 and site_id = 500514; ";             
echo $sql_stmt;
echo "\n";
$statement = $db->prepare($sql_stmt);
$statement->execute();
/*******end of facility update for securecare*******/



/********start facility detailed info update*******/
$site_id_list = array();
//$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
$sql = "select * from facility where company_id = 1";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    array_push($site_id_list, $facility['site_id']);
}

$result_display = getSiteDetails($site_id_list);
$result_detail = $result_display->Details->SOA_SITE_ATTRIBUTES;

//print_r($result_detail);

foreach ( $result_detail as $idetail) {
    $here_site_id = $idetail->SITE_ID;
    $here_site_hours = $idetail->SITE_HOURS;
    $here_gate_hours = $idetail->GATE_HOURS;
    $update_hours_sql = "update facility set office_hours = '{$here_site_hours}', access_hours = '{$here_gate_hours}' 
                        where site_id = {$here_site_id} and company_id =1";
    $statement = $db->prepare($update_hours_sql);
    $statement->execute();                        
                        
}
/*******enc facility detailed info update*******/

/*******start facility rate info update*******/
$sql = "select * from facility where company_id = 1";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    $result_display = GetUnitData($facility['site_id']); 
    $unit_list = $result_display->Details->APPL_RENTAL_OBJECTS_DETAIL;
    $min_price = 10000000; 
    $max_price = 0;
    foreach ($unit_list as $iunit) {
    
        if ($iunit->RENT_RATE < $min_price) {$min_price =$iunit->RENT_RATE; }
        if ($iunit->RENT_RATE > $max_price) {$max_price =$iunit->RENT_RATE; }
    } 
    if ($min_price == 10000000) {$min_price = 0;} 
    $sql_update = "update facility set min_price = {$min_price}, max_price = {$max_price} where id = {$facility['id']} and company_id = 1"; 
    $statement = $db->prepare($sql_update);
    $statement->execute();
}
/*******end facility rate info update*******/

?>