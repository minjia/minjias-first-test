<?php
function getSoapInstance()
{ 
    $soap_singleton = NULL; 
    if ( ! $soap_singleton )
      $soap_singleton = new SoapClient("https://slc.centershift.com/store40/SWS.asmx?WSDL", array('trace' => TRUE));
    return $soap_singleton;
}
function doRequest($name, $params = array())
{
    $params = array('LookupUser_Request' => array(
                      'Username' => 'SS3Golocal',
                      'Password' => 'B38%kqy',
                      'Channel'  => 1),
                      'Request'  => $params);


    try
    {
      $resp = getSoapInstance()->$name($params);
    }
    catch ( Exception $e )
    {
        echo "Error!";
        echo $e -> getMessage ();
        echo 'Last response: '. getSoapInstance()->__getLastResponse(); 
    }



    $result_name = $name .'Result';
    return $resp->$result_name;
}

function getOrgList()
{
  $res = doRequest('GetOrgList');
  return $res;
}

function getSiteList($org_id = NULL)
{
    if ( ! $org_id )
    {
      $org_list = getOrgList();
      $org_id = $org_list->Details->Organization->OrgID;
    }


    $res = doRequest('GetSiteList', array('OrgID' => $org_id));
    return $res;
}

function getUnitData($id, $unit_id=NULL)
{
    $params = array(
    'SiteID' => $id,
    'Active' => 'Y',
    'Status' => 1 //New by JDB on 7/2/2013
    );
    if ( $unit_id )
    $params['UnitID'] = $unit_id;
    $res = doRequest('GetUnitData', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;
}

function getUnitFeatures($id)
{
    $res = doRequest('GetUnitFeatures', array(
    'SiteID' => $id,
    ));
    return $res;
}

function getSingleUnit($site_id, $unit_id)
{

  $res = doRequest('GetUnitData', array(
    'SiteID' => $site_id,
    'UnitID' => $unit_id,
  ));

  return $res->Details->APPL_RENTAL_OBJECTS_DETAIL;
}

function createNewAccount($options)
{
/* Options comes in the the version3 format */
$keys_tr = array(
  'lngSiteID' => 'SiteID',
  'strFirstName' => 'FirstName',
  'strLastName' => 'LastName',
  'strAccountName' => 'AccountName',
  'strAccountType' => 'AccountClass',
);

$account_types = array(
  294 => 'BUSINESS',
  295 => 'PERSONAL',
  562 => 'NATIONAL',
);


$request = array();
foreach ( $keys_tr as $old => $new )
  $request[$new] = $options[$old];


$request['ContactAddress'] = array(array(
  'Addr1' => $options['strAddress1'],
  'Addr2' => $options['strAddress2'],
  'City' => $options['strCity'],
  'State' => $options['strState'],
  'PostalCode' => $options['strPostalCode'],
  'Active' => TRUE,
  'AddrType' => 'HOME',
));

$request['ContactPhone'] = array(array(
  'ACTIVE' => True,
  'PHONE_TYPE' => 1,
  'PHONE' => $options['strHomePhone'],
  /* the docs say these are all required but ignored, set to 0 */
  'ACCT_ID' => 0,
  'CONTACT_ID' => 0,
  'PHONE_ID' => 0,
  'CREATED_BY' => 0,
  'UPDATED_BY' => 0,
));

$request['AccountClass'] = $account_types[$request['AccountClass']];
$request['ContactType'] = 'ACCOUNT_USER';


$org_list = getOrgList();
$request['OrgID'] = $org_list->Details->Organization->OrgID;


$resp = doRequest('CreateNewAccount', $request);

/* v3 compat */
$resp->ACCOUNT_ID = $resp->AccountID;
$resp->CONTACT_ID = $resp->ContactID;
$resp->PASSWORD = '';

return $resp;
}


function makeReservation($options)
{
$keys_tr = array(
  'lngSiteID' => 'SiteID',
  'account_id' => 'AcctID',
  'unit_id' => 'UnitID',
  'strReservation_Start_Date' => 'QuoteStartDate',
);

$request = array();
$request['SiteID'] = $options['lngSiteID'];
$request['UnitID'] = $options['unit_id'];
$request['AcctID'] = $options['account_id'];
$request['ContactID'] = $options['lngContactID'];

$unit_data = getSingleUnit($request['SiteID'], $request['UnitID']);

$request['Version'] = $unit_data->VERSION; 
$request['QuoteType'] = 'HardReservation';
$request['RentNow'] = FALSE;
$request['QuoteExpiration'] = date('c', strtotime('7 days'));


$address = getContactAddress($request['AcctID'], $options['lngContactID']);
$phone = getContactPhone($request['AcctID'], $options['lngContactID']);

/* nope, you can't pass these directly back */
$request['Contacts'] = array(
  array(
    'ContactId' => $address->CONTACT_ID,
    'AddressId' => $address->ADDR_ID,
    'PhoneId' => $phone->PHONE_ID,
    'PrimaryFlag' => TRUE,
  ),
);

return doRequest('MakeReservation', $request);
} 

function getContactAddress($account_id, $contact_id)
{
  $res = doRequest('GetContactAddresses', array(
    'AcctID' => $account_id,
    'ContactID' => $contact_id,
  ));


return $res->Details->ACCT_CONTACT_ADDRESSES;
}


function getContactPhone($account_id, $contact_id)
{
  $res = doRequest('GetContactPhoneNumbers', array(
    'AcctID' => $account_id,
    'ContactID' => $contact_id,
  ));


return $res->Details->ACCT_CONTACT_PHONES;
}





function getCoordinatesFromAddress( $sQuery, $sCountry = 'usa' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($sQuery).'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}

function getAddressFromCoordinates( $dLatitude, $dLongitude, $sCountry = 'it' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$dLatitude,$dLongitude").'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}

$params = array(
  'lngSiteID' => 500679, #Site ID for the site having a new account created
  'strFirstName' => "Test", #Must not be blank.
  'strLastName' => "Golocal", #Must not be blank.
  'strAccountName' => "Test Golocal", #Must not be blank.
  'strAccountType' => "562", #294 = Business, 295 = Individual, 562 = Both
  'strAddress1' => "10880 Benson Drive", #Must not be blank.
  'strAddress2' => "Suite 2300", #Can be blank
  'strCity' => "Overland Park", #Required, entered by online user.
  'strState' => "KS", #Required, entered by online user.
  'strPostalCode' => "66223", #Required, entered by online user.
  'strEmail' => "minjia.tang@golocalinteractive.com", #Required, entered by online user.
  'strReservation_Start_Date' => "12/12/2013", #Required, entered by online user.
  'strHomePhone' => "913-689-3170", #Required, entered by online user. Required 000–000–0000 format.
);
$account = createNewAccount($params);
$account_id = $account->AccountID;
$contact_id = $account->ContactID;

print_r($account);
echo "\n";
echo $account_id;
echo "\n";
echo $contact_id;
echo "\n";


$options['lngSiteID'] = 500679;
$options['unit_id'] = 2469926;
$options['account_id'] = $account_id;
$options['lngContactID'] = $contact_id;
$result_display = makeReservation($options);

print_r($result_display);
//$result_display = doRequest('GetOrgList');

//print_r($result_display);

//$result_display = doRequest('GetSiteList', array('OrgID' => 5041));
//print_r($result_display);

/*
      $params = array(
        'SiteID' => 500679,
        'Active' => 'Y'
      );
$result_display = doRequest('GetSiteUnitDataV2', $params);
*/
    //4133360

//$result_display = getSingleUnit(1000001109, 4133360);
//$result_display = GetUnitData(500679);
//$unit_list = $result_display->Details->APPL_RENTAL_OBJECTS_DETAIL;
//$result_display = getUnitFeatures(1000001109);
//$unit_list = $result_display->SOA_UNIT_FEATURES;
//print_r($result_display->SOA_UNIT_FEATURES);
//print_r($unit_list);
//print_r($result_display);

/*
$site_list = $result_display->Details->SOA_GET_SITE_LIST;
foreach ($site_list as $isite) {
    echo $isite->DISPLAY_NAME;
    echo "\n";
}
*/


?>