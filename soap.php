<?php
function getSoapInstance()
{ 
    $soap_singleton = NULL; 
    if ( ! $soap_singleton )
      $soap_singleton = new SoapClient("https://slc.centershift.com/store40/SWS.asmx?WSDL", array('trace' => TRUE));
    return $soap_singleton;
}
function doRequest($name, $params = array())
{
    $params = array('LookupUser_Request' => array(
                      'Username' => 'SS3Golocal',
                      'Password' => 'B38%kqy',
                      'Channel'  => 1),
                      'Request'  => $params);


    try
    {
      $resp = getSoapInstance()->$name($params);
    }
    catch ( Exception $e )
    {
        echo "Error!";
        echo $e -> getMessage ();
        echo 'Last response: '. getSoapInstance()->__getLastResponse();
    }



    $result_name = $name .'Result';
    return $resp->$result_name;
}

function getOrgList()
{
  $res = doRequest('GetOrgList');
  return $res;
}

function getSiteList($org_id = NULL)
{
    if ( ! $org_id )
    {
      $org_list = getOrgList();
      $org_id = $org_list->Details->Organization->OrgID;
    }


    $res = doRequest('GetSiteList', array('OrgID' => $org_id));
    return $res;
}

function getUnitData($id, $unit_id=NULL)
{
    $params = array(
    'SiteID' => $id,
    'Active' => 'Y',
    'Status' => 1 //New by JDB on 7/2/2013
    );
    if ( $unit_id )
    $params['UnitID'] = $unit_id;
    $res = doRequest('GetUnitData', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;
}

function getUnitFeatures($id)
{
    $res = doRequest('GetUnitFeatures', array(
    'SiteID' => $id,
    ));
    return $res;
}

function getSingleUnit($site_id, $unit_id)
{
  $res = doRequest('GetUnitData', array(
    'SiteID' => $site_id,
    'UnitID' => $unit_id,
  ));

  return $res;
}

function createNewAccount($options)
{
/* Options comes in the the version3 format */
$keys_tr = array(
  'lngSiteID' => 'SiteID',
  'strFirstName' => 'FirstName',
  'strLastName' => 'LastName',
  'strAccountName' => 'AccountName',
  'strAccountType' => 'AccountClass',
);

$account_types = array(
  294 => 'BUSINESS',
  295 => 'PERSONAL',
  562 => 'NATIONAL',
);


$request = array();
foreach ( $keys_tr as $old => $new )
  $request[$new] = $options[$old];


$request['ContactAddress'] = array(array(
  'Addr1' => $options['strAddress1'],
  'Addr2' => $options['strAddress2'],
  'City' => $options['strCity'],
  'State' => $options['strState'],
  'PostalCode' => $options['strPostalCode'],
  'Active' => TRUE,
  'AddrType' => 'HOME',
));

$request['ContactPhone'] = array(array(
  'ACTIVE' => True,
  'PHONE_TYPE' => 1,
  'PHONE' => $options['strHomePhone'],
  /* the docs say these are all required but ignored, set to 0 */
  'ACCT_ID' => 0,
  'CONTACT_ID' => 0,
  'PHONE_ID' => 0,
  'CREATED_BY' => 0,
  'UPDATED_BY' => 0,
));

$request['AccountClass'] = $account_types[$request['AccountClass']];
$request['ContactType'] = 'ACCOUNT_USER';


$org_list = getOrgList();
$request['OrgID'] = $org_list->Details->Organization->OrgID;


$resp = doRequest('CreateNewAccount', $request);

/* v3 compat */
$resp->ACCOUNT_ID = $resp->AccountID;
$resp->CONTACT_ID = $resp->ContactID;
$resp->PASSWORD = '';

return $resp;
}


function makeReservation($options)
{
/* Same as createNewAccount, convert these from the old options */
/*
$params = array(
  'lngSiteID' => $this->location->cs_id,
  'lngContactID' => $this->contact_id,
  'strReservation_Start_Date' => date('m/d/Y'),
  'strDimensions' => $size,
  'strAttributeCode' => $attribute,
  'strCardNumber' => $this['card_number'],
  'strExpDateMonth' => $this['card_expiration_month'],
  'strExpDateYear' => $this['card_expiration_year'],
  'strCardName' => $this['card_name'],
  'strAmount' => '0',
  'strStreet' => $this->address_1,
  'strCity' => $this->city,
  'strState' => $this->state,
  'strZip' => $this->postal_code,
  'strEmail' => $this->email,
);
 */

$keys_tr = array(
  'lngSiteID' => 'SiteID',
  'account_id' => 'AcctID',
  'unit_id' => 'UnitID',
  'strReservation_Start_Date' => 'QuoteStartDate',
);

$request = array();
$request['SiteID'] = $options['lngSiteID'];
$request['UnitID'] = $options['unit_id'];
$request['AcctID'] = $options['account_id'];
$request['ContactID'] = $options['lngContactID'];

$unit_data = getSingleUnit($request['SiteID'], $request['UnitID']);

$request['Version'] = $unit_data->VERSION;
$request['QuoteType'] = 'HardReservation';
$request['RentNow'] = FALSE;
$request['QuoteExpiration'] = date('c', strtotime('7 days'));


$address = getContactAddress($request['AcctID'], $options['lngContactID']);
$phone = getContactPhone($request['AcctID'], $options['lngContactID']);

/* nope, you can't pass these directly back */
$request['Contacts'] = array(
  array(
    'ContactId' => $address->CONTACT_ID,
    'AddressId' => $address->ADDR_ID,
    'PhoneId' => $phone->PHONE_ID,
    'PrimaryFlag' => TRUE,
  ),
);

return doRequest('MakeReservation', $request);
}

function getContactAddress($account_id, $contact_id)
{
  $res = doRequest('GetContactAddresses', array(
    'AcctID' => $account_id,
    'ContactID' => $contact_id,
  ));


return $res->Details->ACCT_CONTACT_ADDRESSES;
}


function getContactPhone($account_id, $contact_id)
{
  $res = doRequest('GetContactPhoneNumbers', array(
    'AcctID' => $account_id,
    'ContactID' => $contact_id,
  ));


return $res->Details->ACCT_CONTACT_PHONES;
}





function getCoordinatesFromAddress( $sQuery, $sCountry = 'usa' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($sQuery).'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}

function getAddressFromCoordinates( $dLatitude, $dLongitude, $sCountry = 'it' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$dLatitude,$dLongitude").'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}
/*
$testdata = getCoordinatesFromAddress("15219 maple st, overland park, ks");
print_r($testdata);
echo "\n";
echo $testdata->results[0]->geometry->location->lat. "\n";
echo $testdata->results[0]->geometry->location->lng. "\n";
*/

//$result_display = doRequest('GetOrgList');

//print_r($result_display);

//$result_display = doRequest('GetSiteList', array('OrgID' => 5041));
//print_r($result_display);

/*
      $params = array(
        'SiteID' => 500679,
        'Active' => 'Y'
      );
$result_display = doRequest('GetSiteUnitDataV2', $params);
*/
    //4133360

//$result_display = getSingleUnit(1000001109, 4133360);
//$result_display = GetUnitData(500679);
//$unit_list = $result_display->Details->APPL_RENTAL_OBJECTS_DETAIL;
//$result_display = getUnitFeatures(1000001109);
//$unit_list = $result_display->SOA_UNIT_FEATURES;
//print_r($result_display->SOA_UNIT_FEATURES);
//print_r($unit_list);
//print_r($result_display);

/*
$site_list = $result_display->Details->SOA_GET_SITE_LIST;
foreach ($site_list as $isite) {
    echo $isite->DISPLAY_NAME;
    echo "\n";
}
*/

//start of facility update for securecare
/*
$site_list = $result_display->Details->SOA_GET_SITE_LIST;
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
foreach ($site_list as $isite) {
    $sql_check = "select * from facility where site_id = {$isite->SITE_ID}";
    $statement = $db->prepare($sql_check);
    $statement->execute();
    $results_check = $statement->fetchAll(PDO::FETCH_ASSOC); 
    $results_check_count = count($results_check);
    if ($results_check_count <1) {
        echo $sql_check;
        echo "\n";
        $site_display_name = addslashes($isite->DISPLAY_NAME);
        $site_city = addslashes($isite->CITY);
        $site_address = addslashes($isite->LINE1);
        $sql_insert = "insert into facility (company_id, site_id, display_name, site_number, address, city, state, zip, 
        phone, email, property_type, site_status, online_status, website) 
                    values ('1', '{$isite->SITE_ID}', '{$site_display_name}', '{$isite->SITE_NUMBER}', '{$site_address}', '{$site_city}', '{$isite->STATE}', 
                    '{$isite->POSTAL_CODE}', '{$isite->PHONE}', '{$isite->EMAIL_ADDRESS}', '{$isite->PROPERTY_TYPE}', '{$isite->SITE_STAUTS}', '{$isite->ONLINE_STATUS}',
                    'www.securcareselfstorage.com')";
        echo $sql_insert;
        echo "\n";
        $statement = $db->prepare($sql_insert);
        $statement->execute();  
               
    } else {
        echo "found, no update\n";
    }  
       
}
*/
//end of facility update for securecare


//
/*
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
$sql = "select * from facility where company_id = 1";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    $result_display = GetUnitData($facility['site_id']); 
    $unit_list = $result_display->Details->APPL_RENTAL_OBJECTS_DETAIL;
    $min_price = 1000000; 
    $max_price = 0;
    foreach ($unit_list as $iunit) {
    
        if ($iunit->RENT_RATE < $min_price) {$min_price =$iunit->RENT_RATE; }
        if ($iunit->RENT_RATE > $max_price) {$max_price =$iunit->RENT_RATE; }
    }  
    $sql_update = "update facility set min_price = {$min_price}, max_price = {$max_price} where id = {$facility['id']}"; 
    $statement = $db->prepare($sql_update);
    $statement->execute();
}
*/

/*
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
$sql = "select * from facility where company_id =5 limit 8000, 2000";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    $sql_re_select = "select * from ztca where zip = '{$facility['zip']}'";
    $statement = $db->prepare($sql_re_select);
    $statement->execute();
    $results2 = $statement->fetchAll(PDO::FETCH_ASSOC);
    $lat = $results2[0]['latitude'];
    $long = $results2[0]['longitude'];
    $sql_update = "update facility set latitude = '{$lat}', longitude = '{$long}' where id = {$facility['id']}"; 
    echo $sql_update."\n";
    $statement = $db->prepare($sql_update);
    $statement->execute();
}
*/

// start of geo code update 
$put_string = "";
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
//for ($qcount=0; $qcount <5; $qcount++) {
    $current_start = $qcount*2000; 
    //$sql = "select * from facility where company_id =5 limit {$current_start}, 2000";
    $sql = "select * from facility where id in (9127,13493,15312,17260)";
    //$sql = "select * from facility where company_id=5 and (latitude is null or latitude ='')";
    $statement = $db->prepare($sql);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $facility) {
        $iaddress = $facility['address'].", ".$facility['city'].", ".$facility['state']." ".$facility['zip'];
        $testdata = getCoordinatesFromAddress($iaddress); 
        $ilat =  $testdata->results[0]->geometry->location->lat;
        $ilng =  $testdata->results[0]->geometry->location->lng;      
        //print_r($testdata);
        echo "\n";
        if (empty($ilat))  {
            $put_string .= $facility['id'].",";
            //echo $facility['id'].": ". $iaddress."\n";
        } else {
            echo $ilat. "\n";
            echo $ilng. "\n";  
            $sql_update = "update facility set latitude = '{$ilat}', longitude = '{$ilng}' where id = {$facility['id']}"; 
            //echo $sql_update."\n";
            $statement = $db->prepare($sql_update);
            $statement->execute();                 
        }                        



        sleep(5);
    }    
   
//}


$file = 'notin.txt';
// Open the file to get existing content
file_put_contents($file, $put_string);

//end of geo code update
?>