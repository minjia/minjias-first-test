<?php

/**
 * Wrapper for CenterShift's API
 */

class CSApiVersion4
{

  public static $soap_singleton = NULL;

  public static $user = 'SS3Golocal';
  public static $password = 'B38%kqy';


  public static function getSoapInstance()
  {
    if ( ! self::$soap_singleton )
      self::$soap_singleton = new SoapClient("https://slc.centershift.com/store40/SWS.asmx?WSDL", array('trace' => TRUE));
    return self::$soap_singleton;
  }


  public static function doRequest($name, $params = array())
  {
    $params = array('LookupUser_Request' => array(
                      'Username' => self::$user,
                      'Password' => self::$password,
                      'Channel'  => 1),
                      'Request'  => $params);

    try
    {
      $resp = self::getSoapInstance()->$name($params);
    }
    catch ( Exception $e )
    {
      $req = self::getSoapInstance()->__getLastRequest();
      if ( Config::get('behavior') == 'development' )  Timer::stopDebug();
      throw new CentershiftExceptionVersion4($e->faultstring, $name, $params, $req);
    }



    $result_name = $name .'Result';
    return $resp->$result_name;
  }


  public static function getOrgList()
  {
    $cache = 'cs4_org_list';
    if ( $res = Cache::get($cache) )
    {
      return $res;
    }
    else
    {
      $res = self::doRequest('GetOrgList');
        Cache::set($cache, $res, DU::ONE_HOUR * 12);
        return $res;
    }
  }


  public static function getSiteList($org_id = NULL)
  {
    if ( ! $org_id )
    {
      $org_list = self::getOrgList();
      $org_id = $org_list->Details->Organization->OrgID;
    }

    $cache = 'cs4_site_list_'. $org_id;
    if ( $res = Cache::get($cache) )
    {
      return $res;
    }
    else
    {
      $res = self::doRequest('GetSiteList', array('OrgID' => $org_id));
        Cache::set($cache, $res, DU::ONE_HOUR * 12);
        return $res;
    }
  }


  /**
   * This method used to make a 'GetSiteUnitData' call to the centershift
   * version 4 API.  It was determined that this call was not actually
   * returning all available units for a location.  Because of this, the
   * 'GetSiteUnitData' call was replaced with 'GetUnitData' call, which returns
   * a more complete data set.
   */
  public static function getSiteUnitData($id)
  {
    $units = array();

    foreach ( self::getUnitData($id) as $data )
      $units[] = CentershiftUnit::version4($data);

    return $units;
  }

  public static function getUnitFeatures($id)
  {
    $cache = 'cs4_site_unit_features_'. $id;
    if ( $res = Cache::get($cache) )
    {
      /* pass */
    }
    else
    {
      $res = self::doRequest('GetUnitFeatures', array(
        'SiteID' => $id,
      ));
      Cache::set($cache, $res, DU::ONE_HOUR * 12);
    }

    $soa_unit_features = self::resultToArray($res->Details->SOA_UNIT_FEATURES);

    $units = array();
    foreach ( $soa_unit_features as $data )
    {
      $unit = CentershiftUnit::version4($data);
      $unit->vacant = FALSE; // we have no way of knowing yet
      $units[] = $unit;
    }
    return $units;
  }


  public static function getUnitData($id, $unit_id=NULL)
  {
      $params = array(
        'SiteID' => $id,
        'Active' => 'Y',
        'Status' => 1 //New by JDB on 7/2/2013
      );
      if ( $unit_id )
        $params['UnitID'] = $unit_id;
      $res = self::doRequest('GetUnitData', $params); //JDB switched to GetUnitData on 7/2/2013 with new param


    return self::resultToArray($res->Details->APPL_RENTAL_OBJECTS_DETAIL);
  }


  static public function getSingleUnit($site_id, $unit_id)
  {
      $res = self::doRequest('GetUnitData', array(
        'SiteID' => $site_id,
        'UnitID' => $unit_id,
      ));


    return $res->Details->APPL_RENTAL_OBJECTS_DETAIL;
  }


  public static function createNewAccount($options)
  {
    /* Options comes in the the version3 format */
    $keys_tr = array(
      'lngSiteID' => 'SiteID',
      'strFirstName' => 'FirstName',
      'strLastName' => 'LastName',
      'strAccountName' => 'AccountName',
      'strAccountType' => 'AccountClass',
    );

    $account_types = array(
      294 => 'BUSINESS',
      295 => 'PERSONAL',
      562 => 'NATIONAL',
    );


    $request = array();
    foreach ( $keys_tr as $old => $new )
      $request[$new] = $options[$old];


    $request['ContactAddress'] = array(array(
      'Addr1' => $options['strAddress1'],
      'Addr2' => $options['strAddress2'],
      'City' => $options['strCity'],
      'State' => $options['strState'],
      'PostalCode' => $options['strPostalCode'],
      'Active' => TRUE,
      'AddrType' => 'HOME',
    ));

    $request['ContactPhone'] = array(array(
      'ACTIVE' => True,
      'PHONE_TYPE' => 1,
      'PHONE' => $options['strHomePhone'],
      /* the docs say these are all required but ignored, set to 0 */
      'ACCT_ID' => 0,
      'CONTACT_ID' => 0,
      'PHONE_ID' => 0,
      'CREATED_BY' => 0,
      'UPDATED_BY' => 0,
    ));

    $request['AccountClass'] = $account_types[$request['AccountClass']];
    $request['ContactType'] = 'ACCOUNT_USER';


    $org_list = self::getOrgList();
    $request['OrgID'] = $org_list->Details->Organization->OrgID;


    $resp = self::doRequest('CreateNewAccount', $request);

    /* v3 compat */
    $resp->ACCOUNT_ID = $resp->AccountID;
    $resp->CONTACT_ID = $resp->ContactID;
    $resp->PASSWORD = '';

    return $resp;
  }


  public static function makeReservation($options)
  {
    Cache::delete('cs4_site_unit_data_'. $options['lngSiteID']);

    /* Same as createNewAccount, convert these from the old options */
    /*
    $params = array(
      'lngSiteID' => $this->location->cs_id,
      'lngContactID' => $this->contact_id,
      'strReservation_Start_Date' => date('m/d/Y'),
      'strDimensions' => $size,
      'strAttributeCode' => $attribute,
      'strCardNumber' => $this['card_number'],
      'strExpDateMonth' => $this['card_expiration_month'],
      'strExpDateYear' => $this['card_expiration_year'],
      'strCardName' => $this['card_name'],
      'strAmount' => '0',
      'strStreet' => $this->address_1,
      'strCity' => $this->city,
      'strState' => $this->state,
      'strZip' => $this->postal_code,
      'strEmail' => $this->email,
    );
     */

    $keys_tr = array(
      'lngSiteID' => 'SiteID',
      'account_id' => 'AcctID',
      'unit_id' => 'UnitID',
      'strReservation_Start_Date' => 'QuoteStartDate',
    );

    $request = array();
    $request['SiteID'] = $options['lngSiteID'];
    $request['UnitID'] = $options['unit_id'];
    $request['AcctID'] = $options['account_id'];
    $request['ContactID'] = $options['lngContactID'];

    $unit_data = self::getSingleUnit($request['SiteID'], $request['UnitID']);

    $request['Version'] = $unit_data->VERSION;
    $request['QuoteType'] = 'HardReservation';
    $request['RentNow'] = FALSE;
    $request['QuoteExpiration'] = date('c', strtotime('7 days'));


    $address = self::getContactAddress($request['AcctID'], $options['lngContactID']);
    $phone = self::getContactPhone($request['AcctID'], $options['lngContactID']);

    /* nope, you can't pass these directly back */
    $request['Contacts'] = array(
      array(
        'ContactId' => $address->CONTACT_ID,
        'AddressId' => $address->ADDR_ID,
        'PhoneId' => $phone->PHONE_ID,
        'PrimaryFlag' => TRUE,
      ),
    );

    return self::doRequest('MakeReservation', $request);
  }


  static public function getContactAddress($account_id, $contact_id)
  {
      $res = self::doRequest('GetContactAddresses', array(
        'AcctID' => $account_id,
        'ContactID' => $contact_id,
      ));


    return $res->Details->ACCT_CONTACT_ADDRESSES;
  }


  static public function getContactPhone($account_id, $contact_id)
  {
      $res = self::doRequest('GetContactPhoneNumbers', array(
        'AcctID' => $account_id,
        'ContactID' => $contact_id,
      ));


    return $res->Details->ACCT_CONTACT_PHONES;
  }

  /**
   * The 'Governor Date' is how many days in advance a reservation can be made.
   */
  static public function getGovernorDate($site_id)
  {
      $res = self::doRequest('GetSiteRuleValue', array(
        'SiteID' => $site_id,
        'RuleID' => 330, // Hard Res - Expiration Days
      ));


    return $res->Details;
  }

  /**
   * Used to ensure that result sets always come through as arrays.  In some
   * cases, single result sets come throught as a single object.
   */
  static public function resultToArray($r)
  {
    return ( is_object($r) ) ? array($r) : $r;
  }

}




//$result_display = doRequest('GetOrgList');

//print_r($result_display);

//$result_display = doRequest('GetSiteList', array('OrgID' => 5041));
//print_r($result_display);

//      $params = array(
//        'SiteID' => 701503,
//        'Active' => 'Y'
//      );
//$result_display = doRequest('GetSiteUnitDataV2', $params);
    //4016018

//$new_obj = new   CSApiVersion4;
//$result_display = $new_obj->GetUnitData(1000001109);
//print_r($result_display);

/*
$site_list = $result_display->Details->SOA_GET_SITE_LIST;
foreach ($site_list as $isite) {
    echo $isite->DISPLAY_NAME;
    echo "\n";
}
*/

/*
$site_list = $result_display->Details->SOA_GET_SITE_LIST;
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
foreach ($site_list as $isite) {
    $sql_insert = "insert into facility_scss (company_id, site_id, display_name, site_number, address, city, state, zip, 
    phone, email, property_type, site_status, online_status, website) 
                values ('1', '{$isite->SITE_ID}', '{$isite->DISPLAY_NAME}', '{$isite->SITE_NUMBER}', '{$isite->LINE1}', '{$isite->CITY}', '{$isite->STATE}', 
                '{$isite->POSTAL_CODE}', '{$isite->PHONE}', '{$isite->EMAIL_ADDRESS}', '{$isite->PROPERTY_TYPE}', '{$isite->SITE_STAUTS}', '{$isite->ONLINE_STATUS}',
                'www.securcareselfstorage.com')";
                //echo $sql_insert;
                //echo "\n";
    $statement = $db->prepare($sql_insert);
    $statement->execute();        
}
*/

/*
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
$sql = "select * from facility_scss";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    $result_display = GetUnitData($facility['site_id']);
    $unit_list = $result_display->Details->APPL_RENTAL_OBJECTS_DETAIL;
    $min_price = 1000000; 
    $max_price = 0;
    foreach ($unit_list as $iunit) {
        if ($iunit->PUSH_RATE < $min_price) {$min_price =$iunit->PUSH_RATE; }
        if ($iunit->PUSH_RATE > $max_price) {$max_price =$iunit->PUSH_RATE; }
    }  
    $sql_update = "update facility_scss set min_price = {$min_price}, max_price = {$max_price} where id = {$facility['id']}"; 
    $statement = $db->prepare($sql_update);
    $statement->execute();
}
*/

?>