<?php
function getSoapInstance()
{ 
    $soap_singleton = NULL; 
    if ( ! $soap_singleton )
      $soap_singleton = new SoapClient("https://slc.centershift.com/store40/SWS.asmx?WSDL", array('trace' => TRUE));
    return $soap_singleton;
}
function doRequest($name, $params = array())
{
    $params = array('LookupUser_Request' => array(
                      'Username' => 'SS3Golocal',
                      'Password' => 'B38%kqy',
                      'Channel'  => 1),
                      'Request'  => $params);


    try
    {
      $resp = getSoapInstance()->$name($params);
    }
    catch ( Exception $e )
    {
        echo "Error!";
        echo $e -> getMessage ();
        echo 'Last response: '. getSoapInstance()->__getLastResponse();
    }



    $result_name = $name .'Result';
    return $resp->$result_name;
}

function getOrgList()
{
  $res = doRequest('GetOrgList');
  return $res;
}

function getSiteList($org_id = NULL)
{
    if ( ! $org_id )
    {
      $org_list = getOrgList();
      $org_id = $org_list->Details->Organization->OrgID;
    }


    $res = doRequest('GetSiteList', array('OrgID' => $org_id));
    return $res;
}

function getSiteUnitData($id)
{
$units = array();

foreach ( self::getUnitData($id) as $data )
  $units[] = CentershiftUnit::version4($data);

return $units;
}

function getSiteDetails($ids_list=array())
{
    $res = doRequest('GetSiteDetails', array('SiteID' => $ids_list));
    return $res;
}
  
function getUnitData($id, $unit_id=NULL)
{
    $params = array(
    'SiteID' => $id,
    'Active' => 'Y',
    'Status' => 1 //New by JDB on 7/2/2013
    );
    if ( $unit_id )
    $params['UnitID'] = $unit_id;
    $res = doRequest('GetUnitData', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;
}

function getUnitFeatures($id)
{
    $res = doRequest('GetUnitFeatures', array(
    'SiteID' => $id,
    ));
    return $res;
}

function getSingleUnit($site_id, $unit_id)
{
  $res = doRequest('GetUnitData', array(
    'SiteID' => $site_id,
    'UnitID' => $unit_id,
  ));

  return $res;
}

function getCoordinatesFromAddress( $sQuery, $sCountry = 'usa' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($sQuery).'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}

function getAddressFromCoordinates( $dLatitude, $dLongitude, $sCountry = 'it' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$dLatitude,$dLongitude").'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}
/*
$testdata = getCoordinatesFromAddress("15219 maple st, overland park, ks");
print_r($testdata);
echo "\n";
echo $testdata->results[0]->geometry->location->lat. "\n";
echo $testdata->results[0]->geometry->location->lng. "\n";
*/

//$result_display = doRequest('GetOrgList');

//print_r($result_display);

//$result_display = doRequest('GetSiteDetails', array('SiteID' => array(500679) ));
//print_r($result_display);

/*
      $params = array(
        'SiteID' => 500679,
        'Active' => 'Y'
      );
$result_display = doRequest('GetSiteUnitDataV2', $params);
*/
    //4133360

//$result_display = getSingleUnit(1000001109, 4133360);
//$result_display = GetUnitData(500679);
//$unit_list = $result_display->Details->APPL_RENTAL_OBJECTS_DETAIL;
//$result_display = getUnitFeatures(1000001109);
//$unit_list = $result_display->SOA_UNIT_FEATURES;
//print_r($result_display->SOA_UNIT_FEATURES);
//print_r($unit_list);
//print_r($result_display);

/*
$site_list = $result_display->Details->SOA_GET_SITE_LIST;
foreach ($site_list as $isite) {
    echo $isite->DISPLAY_NAME;
    echo "\n";
}
*/

//start of facility update for securecare
/*
$site_list = $result_display->Details->SOA_GET_SITE_LIST;
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
foreach ($site_list as $isite) {
    $sql_check = "select * from facility where site_id = {$isite->SITE_ID}";
    $statement = $db->prepare($sql_check);
    $statement->execute();
    $results_check = $statement->fetchAll(PDO::FETCH_ASSOC); 
    $results_check_count = count($results_check);
    if ($results_check_count <1) {
        echo $sql_check;
        echo "\n";
        $site_display_name = addslashes($isite->DISPLAY_NAME);
        $site_city = addslashes($isite->CITY);
        $site_address = addslashes($isite->LINE1);
        $sql_insert = "insert into facility (company_id, site_id, display_name, site_number, address, city, state, zip, 
        phone, email, property_type, site_status, online_status, website) 
                    values ('1', '{$isite->SITE_ID}', '{$site_display_name}', '{$isite->SITE_NUMBER}', '{$site_address}', '{$site_city}', '{$isite->STATE}', 
                    '{$isite->POSTAL_CODE}', '{$isite->PHONE}', '{$isite->EMAIL_ADDRESS}', '{$isite->PROPERTY_TYPE}', '{$isite->SITE_STAUTS}', '{$isite->ONLINE_STATUS}',
                    'www.securcareselfstorage.com')";
        echo $sql_insert;
        echo "\n";
        $statement = $db->prepare($sql_insert);
        $statement->execute();  
               
    } else {
        echo "found, no update\n";
    }  
       
}
*/
//end of facility update for securecare


//

$site_id_list = array();
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
$sql = "select * from facility where company_id = 1";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    array_push($site_id_list, $facility['site_id']);
}

$result_display = getSiteDetails($site_id_list);
$result_detail = $result_display->Details->SOA_SITE_ATTRIBUTES;

print_r($result_detail);

foreach ( $result_detail as $idetail) {
    $here_site_id = $idetail->SITE_ID;
    $here_site_hours = $idetail->SITE_HOURS;
    $here_gate_hours = $idetail->GATE_HOURS;
    $update_hours_sql = "update facility set office_hours = '{$here_site_hours}', access_hours = '{$here_gate_hours}' 
                        where site_id = {$here_site_id} and company_id =1";
    $statement = $db->prepare($update_hours_sql);
    $statement->execute();                        
                        
}


/*
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
$sql = "select * from facility where company_id =5 limit 8000, 2000";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    $sql_re_select = "select * from ztca where zip = '{$facility['zip']}'";
    $statement = $db->prepare($sql_re_select);
    $statement->execute();
    $results2 = $statement->fetchAll(PDO::FETCH_ASSOC);
    $lat = $results2[0]['latitude'];
    $long = $results2[0]['longitude'];
    $sql_update = "update facility set latitude = '{$lat}', longitude = '{$long}' where id = {$facility['id']}"; 
    echo $sql_update."\n";
    $statement = $db->prepare($sql_update);
    $statement->execute();
}
*/



/*
// start of geo code update 
$put_string = "";
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
//for ($qcount=0; $qcount <5; $qcount++) {
    $current_start = $qcount*2000; 
    //$sql = "select * from facility where company_id =5 limit {$current_start}, 2000";
    $sql = "select * from facility where id in (9127,13493,15312,17260)";
    //$sql = "select * from facility where company_id=5 and (latitude is null or latitude ='')";
    $statement = $db->prepare($sql);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $facility) {
        $iaddress = $facility['address'].", ".$facility['city'].", ".$facility['state']." ".$facility['zip'];
        $testdata = getCoordinatesFromAddress($iaddress); 
        $ilat =  $testdata->results[0]->geometry->location->lat;
        $ilng =  $testdata->results[0]->geometry->location->lng;      
        //print_r($testdata);
        echo "\n";
        if (empty($ilat))  {
            $put_string .= $facility['id'].",";
            //echo $facility['id'].": ". $iaddress."\n";
        } else {
            echo $ilat. "\n";
            echo $ilng. "\n";  
            $sql_update = "update facility set latitude = '{$ilat}', longitude = '{$ilng}' where id = {$facility['id']}"; 
            //echo $sql_update."\n";
            $statement = $db->prepare($sql_update);
            $statement->execute();                 
        }                        



        sleep(5);
    }    
   
//}


$file = 'notin.txt';
// Open the file to get existing content
file_put_contents($file, $put_string);

//end of geo code update

*/
?>