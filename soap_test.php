<?php
function getSoapInstance()
{ 
    $soap_singleton = NULL; 
    if ( ! $soap_singleton )
      $soap_singleton = new SoapClient("https://slc.centershift.com/store40/SWS.asmx?WSDL", array('trace' => TRUE));
    return $soap_singleton;
}
function doRequest($name, $params = array())
{
    /*
    $params = array('LookupUser_Request' => array(
                      'Username' => 'ms1ginteractive',
                      'Password' => 'Metr0GoL$',
                      'Channel'  => 8),
                      'Request'  => $params);
                      */
    $params = array('LookupUser_Request' => array(
                      'Username' => 'ms1ginteractive',
                      'Password' => 'Metr0GoL$',
                      'Channel'  => 8),
                      'Request'  => $params);


    try
    {
      $resp = getSoapInstance()->$name($params);
    }
    catch ( Exception $e )
    {
        echo "Error!";
        echo $e -> getMessage ();
        echo 'Last response: '. getSoapInstance()->__getLastResponse();
    }



    $result_name = $name .'Result';
    return $resp->$result_name;
}

function getOrgList()
{
  $res = doRequest('GetOrgList');
  return $res;
}

function getSiteList($org_id = NULL)
{
    if ( ! $org_id )
    {
      $org_list = getOrgList();
      $org_id = $org_list->Details->Organization->OrgID;
    }


    $res = doRequest('GetSiteList', array('OrgID' => $org_id, 'ActiveOnly' => FALSE));
    return $res;
}

function getAvailableDiscounts($site_id, $unit_id, $org_id = NULL)
{
    if ( ! $org_id )
    {
      $org_list = getOrgList();
      $org_id = $org_list->Details->Organization->OrgID;
    }
    $params = array(
    'SiteID' => $site_id,
    'UnitID' => $unit_id,
    'OrgID' => $org_id
    );  


    $res = doRequest('GetAvailableDiscounts', $params);
    return $res->Details->APPL_BEST_PCD;
}

function getUnitData($id, $unit_id=NULL)
{
    $params = array(
    'SiteID' => $id,
    'Active' => 'Y',
    'Status' => 1 //New by JDB on 7/2/2013
    );
    if ( $unit_id )
    $params['UnitID'] = $unit_id;
    $res = doRequest('GetUnitData', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;
}

function getSiteAttributes($id)
{
    $res = doRequest('GetSiteAttributes', array(
    'SiteID' => $id,
    ));
    return $res;
}

function getSiteDetails($id)
{
    $id_array[]= $id;
    $res = doRequest('GetSiteDetails', array(
    'SiteID' => $id_array,
    ));
    return $res;
}

function getUnitFeatures($id)
{
    $res = doRequest('GetUnitFeatures', array(
    'SiteID' => $id,
    ));
    return $res;
}

function getSingleUnit($site_id, $unit_id)
{

  $res = doRequest('GetUnitData', array(
    'SiteID' => $site_id,
    'UnitID' => $unit_id,
  ));

  return $res->Details->APPL_RENTAL_OBJECTS_DETAIL;
}

function getSiteUnitData($id, $unit_id=NULL)
{
    $params = array(
    'SiteID' => $id,
    'GetPromoData' => 'Y'
    );
    $res = doRequest('GetSiteUnitData', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;  
} 

function getSiteUnitDataV2($id, $unit_id=NULL)
{
        $params = array(
        'SiteID' => $id,
        'PromoDataType' => 'HighestPriorityPromotion',
        'Active' => 'Y',
        'Status' => 1 //New by JDB on 7/2/2013
        );
    if ( $unit_id )
    $params['UnitID'] = $unit_id;
    $res = doRequest('GetSiteUnitDataV2', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;
} 

function getUnitDataV2($id, $unit_id=NULL)
{
    $params = array(
    'SiteID' => $id,
    'Active' => 'Y',
    'Status' => 1 //New by JDB on 7/2/2013
    );
    if ( $unit_id )
    $params['UnitID'] = $unit_id;
    $res = doRequest('GetUnitDataV2', $params); //JDB switched to GetUnitData on 7/2/2013 with new param
    return $res;
} 

function getCoordinatesFromAddress( $sQuery, $sCountry = 'usa' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($sQuery).'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}

function getAddressFromCoordinates( $dLatitude, $dLongitude, $sCountry = 'it' )
{
    $sURL = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.urlencode("$dLatitude,$dLongitude").'&sensor=false&region='.$sCountry.'&language='.$sCountry;
    $sData = file_get_contents($sURL);
    
    return json_decode($sData);
}
/*
$testdata = getCoordinatesFromAddress("15219 maple st, overland park, ks");
print_r($testdata);
echo "\n";
echo $testdata->results[0]->geometry->location->lat. "\n";
echo $testdata->results[0]->geometry->location->lng. "\n";
*/

//$result_display = doRequest('GetOrgList');

//print_r($result_display);

//$result_display = doRequest('GetSiteList', array('OrgID' => 5041));
/*
$result_display = getSiteList();



      $params = array(
        'SiteID' => 500679,
        'Active' => 'Y'
      );
      */
//$result_display = doRequest('GetSiteUnitDataV2', $params);
    //4133360
    //$result_display = getAvailableDiscounts(702164, 3662757);
    /*
    $result_display = getSiteAttributes(500309);  
    print_r($result_display);
    echo "\n";
    $test_obj =  $result_display->Details->ORG_EXT_ATTRIBUTES;
    echo count($test_obj);
    echo "\n";
    if(count($test_obj)>1)  {
        foreach ($test_obj as $iobj) {
            print_r($iobj->ATT_NAME);
            echo "\n";
        }        
    } else {
        print_r($test_obj->ATT_NAME);
        echo "\n";
    }

    echo "\n";
    */
//$result_display = GetSiteAttributes(500679);
//$result_display = getSingleUnit(500679, 4133360);
//$result_display = getSingleUnit(500679, 2469926);
//$result_display = GetUnitFeatures(500679);
//$result_display = getUnitData(500679);
//$unit_list = $result_display->Details->APPL_RENTAL_OBJECTS_DETAIL;
//$result_display = GetSiteUnitDataV2(1000001109, 4133360);
//$unit_list = $result_display->SOA_UNIT_FEATURES;
//print_r($result_display->SOA_UNIT_FEATURES);
//print_r($unit_list);
$result_display = getSiteList();
print_r($result_display);





?>
