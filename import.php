<?php
$db = new PDO( "mysql:host=localhost;dbname=fls_wordpress","root","4sUbSvBWNG4VbEuV"); 
/*
$sql = "select f.*, c.name from facility f, company c where f.company_id = c.id";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC); 
print_r($results);
*/

$sql = "select * from facility where company_id=3";
$statement = $db->prepare($sql);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
foreach ($results as $facility) {
    $total_unit = rand(25, 300);
    $available_unit = rand(25, round($total_unit/2));
    $unit_name = strtolower($facility['city']."_".$facility['zip']."_unit_");
    
    $usize = array("5x5","5x10","5x15","7.5x10","5x12","5x11","5x8","7x10","10x15","10x12","10x11","10x10","8x20","8.5x10","8x10",
                    "10x14","10x12.5","9x22","10x20","10x30","12x25");
    $uclimate = array("yes", "yes", "no", "yes");
    $uinside =  array("yes", "no", "yes");
    $upower = array("yes", "yes", "no", "yes","yes","yes", "yes", "yes","yes","yes","yes","yes");
    $ualarm = array("yes", "yes", "no", "yes");
    $ufloor = array("type1", "type2", "type1");
    $utypeid = array(1,2,1);
    for($i=0; $i<$total_unit; $i++) {
        $i_unit_name = $unit_name.($i+1);
        $i_climate = $uclimate[array_rand($uclimate)];
        $i_inside = $uinside[array_rand($uinside)];
        $i_power = $upower[array_rand($upower)];
        $i_alarm = $ualarm[array_rand($ualarm)];
        $i_floor = $ufloor[array_rand($ufloor)];
        $i_type_id = $utypeid[array_rand($utypeid)];
        $i_size = $usize[array_rand($usize)];
        $temp_size_array = explode("x", $i_size);
        $i_width = $temp_size_array[0];
        $i_length = $temp_size_array[1];
        switch ($i_size) {
            case "5x5": $i_rate = 40; break;
            case "5x10": $i_rate = 55;  break;
            case "5x15": $i_rate = 60;    break;
            case "7.5x10": $i_rate = 60;     break;
            case "5x12": $i_rate = 60;      break;
            case "5x11": $i_rate = 60;  break;
            case "5x8": $i_rate = 45;    break;
            case "7x10": $i_rate = 60;   break;
            case "10x15": $i_rate = 95;   break;
            case "10x12": $i_rate = 85; break;
            case "10x11": $i_rate = 80;   break;
            case "10x10": $i_rate = 80;  break;
            case "8x20": $i_rate = 100;  break;
            case "8.5x10": $i_rate = 65; break;
            case "8x10": $i_rate = 65;  break;
            case "10x14": $i_rate = 90;   break;
            case "10x12.5": $i_rate = 85; break;
            case "9x22": $i_rate = 110;  break;
            case "10x20": $i_rate = 115;  break;
            case "10x30": $i_rate = 145;  break;
            case "12x25": $i_rate = 145;  break;
            default: $i_rate = 60;     break;
        }

        $sql_insert = "insert into unit (facility_id, unit_type_id, unit_name, width, length, climate_controlled, standard_rate, inside, power, alarm, floor) 
                    values ({$facility['id']}, {$i_type_id}, '{$i_unit_name}', '{$i_width}', '{$i_length}', '{$i_climate}', '{$i_rate}', '{$i_inside}', 
                    '{$i_power}', '{$i_alarm}', '{$i_floor}')";
        $statement = $db->prepare($sql_insert);
        $statement->execute();
      
    }
}
?>